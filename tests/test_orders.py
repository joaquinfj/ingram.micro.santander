from unittest import TestCase

from logic import Order
from .fixtures import sample_order_a, sample_order_b, sample_order_c


class TestOrders(TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.sample_order_a = sample_order_a()
        self.sample_order_b = sample_order_b()
        self.sample_order_c = sample_order_c()

    def test_order_sample_A(self) -> bool:
        """
        :param sample_order_a:
        :return: boolean
        """
        self.assertEqual(2, self.sample_order_a[2]["Cloud Service Three"])
        self.assertTrue(True)
        return True

    def test_final_price_for_order_a(self) -> bool:
        """
        :param sample_order_a:
        :return: boolean
        """
        order_tested = Order(self.sample_order_a)
        self.assertEqual(56.0, order_tested.calculate_final_price())
        return True

    def test_final_price_for_order_b(self) -> bool:
        """
        :param sample_order_a:
        :return: boolean
        """
        order_tested = Order(self.sample_order_b)
        self.assertEqual(8.0, order_tested.calculate_final_price())
        return True

    def test_get_distinct_licenses_a(self):
        order_tested = Order(self.sample_order_a)
        self.assertEqual(6, order_tested.get_distinct_licenses())
        return True

    def test_get_distinct_licenses_b(self):
        order_tested = Order(self.sample_order_b)
        self.assertEqual(1, order_tested.get_distinct_licenses())
        return True

    def test_get_distinct_licenses_c(self):
        order_tested = Order(self.sample_order_c)
        self.assertEqual(2, order_tested.get_distinct_licenses())
        return True
