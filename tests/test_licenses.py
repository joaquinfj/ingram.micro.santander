from unittest import TestCase

from logic import Licenses


class TestLicenses(TestCase):

    def setUp(self) -> None:
        super().setUp()

    @classmethod
    def tearDownClass(cls) -> None:
        super().tearDownClass()

    def test_licenses_json_format(self) -> bool:
        """
        :return: boolean
        """
        subject_licenses = Licenses()
        dict_licenses = subject_licenses.all
        self.assertIsInstance(dict_licenses, dict)
        self.assertEqual(8.00, subject_licenses.get_license_price('Cloud Service Three'))
        self.assertTrue(True)
        return True

    def test_licenses_non_existent(self) -> bool:
        """
        :return: boolean
        """
        subject_licenses = Licenses()
        dict_licenses = subject_licenses.all
        self.assertIsInstance(dict_licenses, dict)
        self.assertEqual(None, subject_licenses.get_license_price('Cloud Service Nine'))
        self.assertTrue(True)
        return True
