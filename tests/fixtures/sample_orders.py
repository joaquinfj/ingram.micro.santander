import json
import os

rel_path = '../../data/'
script_dir = os.path.dirname(__file__)


def sample_order_a():
    abs_file_path = os.path.join(script_dir, rel_path, 'sample_order_A.json')
    with open(abs_file_path) as f_in:
        return json.load(f_in)


def sample_order_b():
    abs_file_path = os.path.join(script_dir, rel_path, 'sample_order_B.json')
    with open(abs_file_path) as f_in:
        return json.load(f_in)


def sample_order_c():
    abs_file_path = os.path.join(script_dir, rel_path, 'sample_order_C.json')
    with open(abs_file_path) as f_in:
        return json.load(f_in)
