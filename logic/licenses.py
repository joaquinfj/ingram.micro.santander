import json
import os


class Licenses:

    license_source = '../data/licenses_prices.json'

    def __init__(self):
        self.all = None

        script_dir = os.path.dirname(__file__)
        abs_file_path = os.path.join(script_dir, Licenses.license_source)
        with open(abs_file_path) as f_in:
            self.all = json.load(f_in)

    def get_license_price(self, license_name):
        """
        :param license_name: str
        :return: float
        """
        if license_name in self.all:
            return self.all[license_name]
        else:
            return None

