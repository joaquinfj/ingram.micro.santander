import pandas as pd

from .licenses import Licenses
from .discounts import Discounts


class Order:
    """
    We use Pandas for the calculations, it helps visualize, pivoting and other stuff.
    """
    def __init__(self, services_purchased):
        """
        :param services_purchased: list
        """
        self.license_prices = Licenses()
        self.discounts = Discounts()
        self.services_purchased = services_purchased
        self.df_order = self.__convert_services_to_pandas_df_order() # type: pd.DataFrame

    def __convert_services_to_pandas_df_order(self):
        """
        :return: Pandas DataFrame
        """
        labels = ['service_name', 'quantity']
        services_purchased = []
        for service_purchased in self.services_purchased:
            license_name = list(service_purchased.keys())[0]
            quantity = service_purchased[license_name]
            service = (license_name, quantity)
            services_purchased.append(service)
        df_order = pd.DataFrame.from_records(services_purchased, columns=labels)
        return df_order

    def __add_original_per_license_price(self, df_order):
        """"
        :return: pd.Series
        """
        return self.df_order['service_name'].apply(self.license_prices.get_license_price)

    def __add_original_price(self, df_order):
        """"
        :return: pd.Series
        """
        return self.df_order['quantity'] * self.df_order['original_per_license_price']

    def __get_price_after_discount(self, df_order):
        """"
        :return: pd.Series
        """
        pd_serie_price = self.df_order['quantity'] * self.df_order['original_per_license_price']
        pd_serie_discount_amount = (pd_serie_price * (self.df_order['discount_perc'] / 100))

        return pd_serie_price - pd_serie_discount_amount

    def calculate_final_price(self):
        """
        :param order: list
        :return: float
        """
        self.df_order['original_per_license_price'] = self.__add_original_per_license_price(self.df_order)
        self.df_order['original_price'] = self.__add_original_price(self.df_order)
        # original_price = self.df_order['original_price'].sum()
        self.df_order['discount_perc'] = self.get_discount_by_number_licenses(self.get_distinct_licenses())
        self.df_order['price_after_discount'] = self.__get_price_after_discount(self.df_order)

        return round(self.df_order['price_after_discount'].sum(), 2)

    def get_distinct_licenses(self):
        """
        :return: int
        """
        count = self.df_order.groupby(['service_name'])['quantity'].count().sum()
        return count

    def get_discount_by_number_licenses(self, number):
        """
        This method should resolve which discounts to use. If we decide to use different combinations, then
          most probably we might have to split the DataFrame into different rows-combinations.
        :param number: int
        :return:  float
        """
        discount = self.discounts.get_discount_price(str(number))
        if discount is None:
            return 0
        else:
            return discount
