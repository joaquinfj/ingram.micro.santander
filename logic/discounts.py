import json
import os


class Discounts:

    discount_source = '../data/discounts.json'

    def __init__(self):
        self.all = None

        script_dir = os.path.dirname(__file__)
        abs_file_path = os.path.join(script_dir, Discounts.discount_source)
        with open(abs_file_path) as f_in:
            self.all = json.load(f_in)

    def get_discount_price(self, discount_number):
        """
        :param discount_number: str
        :return: float
        """
        if discount_number in self.all:
            return self.all[discount_number]
        else:
            return None

