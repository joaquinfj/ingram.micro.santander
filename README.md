
##Joaquín Forcada Jiménez for Ingram Micro

### Exercise

The company X is selling seven different cloud services. The license of each service has a standard price of $8.00. However, if you buy more than one of the services at a time, you get a discount. Buying multiple licenses of the same service does not earn a discount.

#### Number of Different Services Discount Percentage
•	2 -> 5%

•	3 ->10%

•	4 -> 15%

•	5 -> 25%

•	6 -> 30%

•	7 -> 35%


Write a method that will calculate the optimal customer discount for any set of licenses from this services. Keep in mind that larger percentages are not always better as can be seen in the sample below, in which case it was better to keep all the services at 75% discount instead of having some at 70% and others at 85% of retail.

#### Sample Selection
•	1 license of Cloud Service One

•	2 licenses of Cloud Service Two

•	2 licenses of Cloud Service Three

•	2 licenses of Cloud Service Four

•	2 licenses of Cloud Service Five

•	1 license of Cloud Service Six





#### Possible solutions:

> One big question is not clear, once you get a discount, **shouldn't this discount potentially apply to all the licenses**? Can you apply several discount in the same purchase? In my opinion based on the exercise statement and the last remarks, is kind of confusing.
My advice, is you should provide a sample of expected results, so the developer really understands what he has to implement.

 

 
10 Licenses, 6 different. Combinations:


> **A. ((10 x $8.00) - 30%) = $56** * Best

> B. ((6 x $8.00) - 30%) + (4 x $8.00 - 15%) = $60.8

> C. ((5 x $8.00) - 25%) + ((5 x $8.00) - 25%) = $60 

### Installation
Implemented in Python 3.6, list of requirements within the Pipfile. 

Instructions:

> pipenv install

> pipenv shell

 > $> python -m pytest -v
 
 Samples of data are to be found in:
 
 > data/*.json
 
 **You can test other results by adding other sample_orders_X.json files.**
 
 
 
 ### How to run
 
 See screenshots under folder "/screenshots/"
 
 
 >  ![running on PyCharm](screenshots/demo_run_on_pyCharm.png?raw=true)
 
 
 
 
 
 >  ![running CLI](screenshots/demo_run_on_cli.png)
 
 
 ### More information
 
 The solution could be implemented in a not so modularized and complex manners, but this way it might help to change the criteria when it comes to apply discounts.